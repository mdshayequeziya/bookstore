package com.example.bookstore.controller;

import com.example.bookstore.entity.User;
import com.example.bookstore.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;


    @PostMapping
    public User saveUser(@RequestBody User user){
        return userService.saveUser(user);
    }

    @GetMapping("/getall")
    public List<User> getAllUser(){
        return userService.getAllUsers();
    }

    @GetMapping("/get/{userid}")
    public User getUserById(@PathVariable String userid){
        return userService.getUserById(userid);
    }

    @PutMapping("/update")
    public User updateUserById(@RequestBody User user){
        return userService.updateUserByUserName(user);
    }

}
