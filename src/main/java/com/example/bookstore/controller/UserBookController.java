package com.example.bookstore.controller;

import com.example.bookstore.entity.Book;
import com.example.bookstore.entity.User;
import com.example.bookstore.service.UserBookService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/books/users")
public class UserBookController {

    private final UserBookService userBookService;

    @PostMapping("/{username}")
    public User saveBookForTheUser(@RequestBody Book book, @PathVariable String username){

        return userBookService.saveBook(book, username);

    }

    @DeleteMapping("/{bookid}/{username}")
    public boolean deleteBookByBookId(@PathVariable String bookid, @PathVariable String username){
        return userBookService.deleteBookById(bookid, username);
    }

}
