package com.example.bookstore.repositories;

import com.example.bookstore.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {


    User findByUserName(String username);
}
