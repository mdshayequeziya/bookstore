package com.example.bookstore.service;

import com.example.bookstore.entity.Book;
import com.example.bookstore.entity.User;
import com.example.bookstore.repositories.BookRepository;
import com.example.bookstore.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserBookService {

    private final BookRepository bookRepository;
    private final UserRepository userRepository;


    public List<Book> getAllBooksForUser(String username){

        User user = userRepository.findByUserName(username);
        List<Book> books = user.getBooks();

        return books;

    }

    public User saveBook(Book book, String username){

        User user = userRepository.findByUserName(username);
        List<Book> books = user.getBooks();
        Book saved = bookRepository.save(book);
        books.add(saved);
        user.setBooks(books);

        return userRepository.save(user);

    }

    public boolean deleteBookById(String bookId, String username){
        User user = userRepository.findByUserName(username);

        user.getBooks().removeIf(x-> x.getId().equals(bookId));
        userRepository.save(user);

        bookRepository.deleteById(bookId);
        return true;
    }
}
