package com.example.bookstore.service;

import com.example.bookstore.entity.User;
import com.example.bookstore.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public User saveUser(User user) {

        return userRepository.save(user);
    }

    public List<User> getAllUsers(){
        return userRepository.findAll();
    }

    public User getUserById(String userId) {

        return userRepository.findById(userId).orElse(null);
    }


    public User updateUserByUserName(User user){

        User oldUser = userRepository.findByUserName(user.getUserName());
        if(oldUser != null){

            oldUser.setUserName(user.getUserName());
            oldUser.setPassword(user.getPassword());
            userRepository.save(oldUser);
        }
        return null;
    }
}
