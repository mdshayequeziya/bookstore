package com.example.bookstore.service;

import com.example.bookstore.entity.Book;
import com.example.bookstore.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {

    private final BookRepository bookRepository;

    @Autowired
    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }


    public Book saveBook(Book book){
        return bookRepository.save(book);
    }

    public ResponseEntity<?> getAllBooks() {

        List<Book> all = bookRepository.findAll();

        return new ResponseEntity<>(all, HttpStatus.OK);
    }
}
