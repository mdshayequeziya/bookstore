package com.ff.mongocrud.repositories;

import com.ff.mongocrud.entity.Book;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BookRepository extends MongoRepository<Book, String> {
}
