package com.ff.mongocrud.entity;

import lombok.Data;
import lombok.NonNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document("users")
@Data
public class User {

    @Id
    private String id;

    @Indexed(unique = true)   // aisa krne se userName field par hamari searching fast ho jaayegi
    @NonNull
    private String userName;

    @NonNull
    private String password;


    // user will have list of journal entries
    // and = new ArrayList() kr rhe so that when
    private List<Book> books= new ArrayList<>();


}
