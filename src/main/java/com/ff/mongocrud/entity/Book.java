package com.ff.mongocrud.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "book_store")
@Data
public class Book {


    @Id
    private String id;

    private String name;
    private String author;
    private String description;


}
